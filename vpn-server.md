
# Настройка vpn на своем сервере

## Ссылки

- Настройка сервера и ios

	https://vc.ru/dev/66942-sozdaem-svoy-vpn-server-poshagovaya-instrukciya
	https://habr.com/ru/post/250859/


- Настройка на андроиде

	https://www.securevpn.pro/rus/setup/android-ikev2-vpn?url=rus%2Fsetup%2Fandroid-ikev2-vpn
	https://www.ivpn.net/setup/android-ipsec-with-ikev2/

- Документация

	https://docs.strongswan.org/strongswan-docs/5.9/interop/ios.html#iOS-client-configuration
    https://www.strongswan.org/docs/readme4.htm
    https://console.kim.sg/strongswan-ipsec-vpn-with-pre-shared-key-and-certificates/
    https://docs.strongswan.org/strongswan-docs/5.9/howtos/forwarding.html

## Предисловие

Где поднимать?

По идее должно работать с любым выделенным или виртуальным сервером. У меня конфигурация минимальная: `1GB Memory / 25GB Storage / 1TB Bandwidth`

Походие конфигурации по цене есть у провайдеров из списка. Но надо смотреть по возможности оплаты. На сколько рационально использовать российских провайдеров, даже при расположении датацентра в Амстердаме и т.п. нужно решать для себя. Цена в $3-5 это дешевле, чем подписка на платный сервис.

- Amazon Web Services (AWS)
- DigitalOcean
- Hetzner

Часть текста будет дублировать [статью](https://vc.ru/dev/66942-sozdaem-svoy-vpn-server-poshagovaya-instrukciya) с vc, но с моими корректировками. Теорию о работе vpn опустим.

Учить, что за собой подчищать и удалять файлы - не буду. Напоминаю, что для генерации надежных паролей стоит использовать утилиту `pwgen`.

## Создаем свой VPN-сервер. Пошаговая инструкция

*Далее курсисвом могут быть мои комментарии по отличиям конфигурации. В основном где лень переписывать абзацы текста.*

### В какой стране поднять свой VPN-сервер
Выбирайте страну для размещения VPN-сервера исходя из следующих критериев:

Кратчайшее расстояние до вас: это обеспечит наименьший пинг и просадки по скорости.
Минимальное количество запретов на свободу Интернета, доступность любых популярных мировых сервисов в этой стране.

Наименьшая политическая напряженность между вашей страной и страной VPN-сервера. В этом случае ваш трафик теперь уже с VPN-сервера не будут читать спецслужбы другого государства. Наверное :) Но здесь палка о двух концах. Например, многие российские пользователи предпочитают располагать VPN-сервер в Великобритании именно из-за высокой напряженности между странами, ведь в случае чего, последняя ни за что не сдаст ваш трафик российским спецслужбам. Поэтому, данный подход также может быть оправданным.

Для российских пользователей в целом подходит любая страна Евросоюза, однако, практика показывает, что лучшим решением является Германия: минимальный пинг, высокая скорость и великолепная доступность любых мировых ресурсов.

### Протоколы VPN-соединения: почему IPsec IKEv2, а не Open VPN

Сегодня существуют разные протоколы VPN-соединения, их детальный разбор выходит за рамки этой статьи. Среди них наиболее популярны IPsec IKEv2 и OpenVPN.

Оба хороши и надежны, но мы будем использовать IKEv2, поскольку у OpenVPN, на мой взгляд, имеется огромный недостаток, который перекрывает его прочие достоинства.

OpenVPN требует установки своего приложения, которое всегда должно быть запущено на устройствах, что, во-первых, неудобно в использовании, а во-вторых, дополнительно жрет процессор и как следствие быстрее расходует батарею iPhone, iPad и, в меньшей степени, Mac.

IKEv2 же вшит в iOS и macOS и является для этих ОС нативным, не требуя установки никакого дополнительного ПО. Так же, считается, что IKEv2 гораздо быстрее и безопасней, чем OpenVPN.

*Для Android я пока вынужден использовать StrongSwan VPN Client. Возможно найду как нативно настроить позже.*

В качестве серверной части мы будем использовать strongSwan — популярный VPN-сервер для Linux.

Готовые скрипты для развертывания VPN-сервера: Algo, Streisand и почему не стоит их использовать

Сегодня существуют готовые решения для развертывания своего VPN-сервера на платформе Linux, например, скрипт Algo (для IKEv2) или Streisand (для OpenVPN), которые нужно просто скачать, распаковать и запустить на сервере. Данные скрипты сами установят и настроят все необходимые пакеты и на выходе предоставят для вас работающий VPN-сервер.

Streisand нам не подходит в принципе, поскольку заточен под OpenVPN. Что же касается Algo, то пробежавшись по диагонали, я увидел, что данный скрипт устанавливает много лишних пакетов, без которых вполне можно обойтись, а также каких-то подозрительных пакетов, неизвестно кем созданных и кем проверенных. Кроме того, Algo устанавливается только на Ubuntu, что нам, опять же, не подходит.

Таким образом, мы будем создавать свой VPN-сервер, используя следующие технологии:

- IKEv2 как протокол VPN
- Linux Debian в качестве серверной ОС *(У меня Ubuntu)*
- strongSwan в качестве VPN-сервера
- никаких готовых скриптов, всё настроим руками.

Итак, с теоретической частью покончено, приступаем к созданию своего VPN-сервера.

### Инструкция по созданию собственного VPN-сервера на базе Linux ~~Debian~~ Ubuntu

#### Регистрация и подключение к серверу

Для каждого провайдера путь примерно одинаковый. У некоторых провайдеров нужно передать публичный ключ для авторизации по ключу. Как создавать пару ключей на windows, osx, linux описывать не буду.

- Смотрим тарифы и способы оплаты, выбираем хостера
- Регистрируемся
- Привязываем способ оплаты
- Создаем экземпляр сервера
- Выбираем операционку
- Передаем/получаем права доступа (pubkey или логин/пароль или pem ключ)
- Можно отключить фаервол на этапе настройки, если он включен по умолчанию. Дальше мы настроим нативный iptables.

#### Подключаемся к серверу

Скорее всего провайдер пришлет письмо с комментариями, как подключаться.

#### Обновим систему

Все манипуляции будем осуществлять из-под пользователя root.

Установим обновления

```sh
apt-get update
apt-get upgrade
```


#### Установим strongSwan и создадим ключи

Установим strongSwan, набор стандартных плагинов, пакет для настройки сертификатов.

```sh
apt-get install strongswan libstrongswan-standard-plugins strongswan-pki
```

Обычно на серверах не установлен редактор `vim`. Если вас это также огорчает, установите его:

```sh
apt-get install vim
```

Далее вместо `YOUR_SERVER_IP` подставляйте внешний IP-адрес машины. Команды можно вводить одна за другой. Я лениывый, поэтому засунул в скрипт.

Теперь нам нужно создать корневой сертификат, он же “CA” (Certificate Authority), который выпустит нам все остальные сертификаты. Создадим его в файле ca.pem. (1)

Далее создадим сертификат для самого VPN-сервера в файле debian.pem (2)

И сертификат для самих устройств в файле me.pem. В этом блоке ничего (в том числе в “CN=me”) менять не нужно. Пример добавления еще одного сертификата в том же блоке.(3)

```sh
cat <<EOF > vpn.sh
#!/bin/bash -x

# Create root cert (1)

cd /etc/ipsec.d
ipsec pki --gen --type rsa --size 4096 --outform pem > private/ca.pem
ipsec pki --self --ca --lifetime 3650 --in private/ca.pem \
  --type rsa --digest sha256 \
  --dn "CN=YOUR_SERVER_IP" \
  --outform pem > cacerts/ca.pem
cd -


# Create server cert (2)

cd /etc/ipsec.d
ipsec pki --gen --type rsa --size 4096 --outform pem > private/debian.pem
ipsec pki --pub --in private/debian.pem --type rsa |
ipsec pki --issue --lifetime 3650 --digest sha256 \
  --cacert cacerts/ca.pem --cakey private/ca.pem \
  --dn "CN=YOUR_SERVER_IP" \
  --san YOUR_SERVER_IP \
  --flag serverAuth --outform pem > certs/debian.pem
cd -

# Create user cert (3)

cd /etc/ipsec.d
ipsec pki --gen --type rsa --size 4096 --outform pem > private/me.pem
ipsec pki --pub --in private/me.pem --type rsa |
ipsec pki --issue --lifetime 3650 --digest sha256 \
  --cacert cacerts/ca.pem --cakey private/ca.pem \
  --dn "CN=me" --san me \
  --flag clientAuth \
  --outform pem > certs/me.pem

ipsec pki --gen --type rsa --size 4096 --outform pem > private/ios_me.pem
ipsec pki --pub --in private/ios_me.pem --type rsa |
ipsec pki --issue --lifetime 3650 --digest sha256 \
  --cacert cacerts/ca.pem --cakey private/ca.pem \
  --dn "CN=ios_me" --san ios_me \
  --flag clientAuth \
  --outform pem > certs/ios_me.pem

cd -

EOF
```

Создаем подписанный ключ для подключения на Android устройстве. Его мы позже экспортируем. Команда потребует ввода пароля для подписи. Можно его передать опцией: `-password pass:[pass]`

```sh
mkdir /etc/ipsec.d/p12
openssl pkcs12 -export -inkey /etc/ipsec.d/private/me.pem -in /etc/ipsec.d/certs/me.pem \
-name "me" -certfile /etc/ipsec.d/cacerts/ca.pem -out /etc/ipsec.d/p12/me.p12
```

Для надежности удалим файл ca.pem, он нам больше не потребуется:

```sh
rm /etc/ipsec.d/private/ca.pem
```

Создание сертификатов завершено.

#### Настроим strongSwan

Очистим дефолтный конфиг strongSwan командой:

```
> /etc/ipsec.conf
```

И создадим свой в текстовом редакторе ~~nano~~ vim:

```vim /etc/ipsec.conf```

Вставьте в него данный текст, заменив YOUR_SERVER_IP на внешний IP-адрес машины. Больше в конфиге ничего менять не нужно. Но при желании можно изучить параметры и добавить необходимое.

```sh
cat <<EOF > /etc/ipsec.conf
config setup
uniqueids=never
	charondebug="ike 2, knl 2, cfg 2, net 2, esp 2, dmn 2,  mgr 2"

conn %default
	keyexchange=ikev2
	ike=aes128gcm16-sha2_256-prfsha256-ecp256!
	esp=aes128gcm16-sha2_256-ecp256!
	fragmentation=yes
	rekey=no
	compress=yes
	dpdaction=clear
	left=%any
	leftauth=pubkey
	leftsourceip=YOUR_SERVER_IP
	leftid=YOUR_SERVER_IP
	leftcert=debian.pem
	leftsendcert=always
	leftsubnet=0.0.0.0/0
	right=%any
	rightauth=pubkey
	rightsourceip=10.10.10.0/24
	rightdns=8.8.8.8,8.8.4.4

conn ikev2-pubkey
	auto=add
EOF
```

**Внимание!** strongSwan требователен к отступам в конфиге, поэтому убедитесь, что параметры каждого раздела конфига отбиты через Tab, как это показано на примере, или хотя бы через пробел, иначе strongSwan не запустится.

Добавим в файл `ipsec.secrets`, который является хранилищем ссылок на сертификаты и ключи аутентификации, указатель на наш сертификат сервера:

```sh
vim /etc/ipsec.secrets
```
Вставим в этот файл последней строкой указатель на наш сертификат сервера (да, прям вот так, начиная с двоеточия):

`: RSA debian.pem`

На этом настройка Strongswan завершена, можно рестартнуть службу:

```sh
ipsec restart
```

Если все хорошо, то сервер запустится:

> ...
Starting strongSwan 5.7.2 IPsec [starter]...

Если упадет в ошибку, то можно посмотреть, что именно произошло, почитав логи. Команда выведет 50 последних строк лога:

```
tail -n 50 > /var/log/syslog
```

#### Настроим сетевые параметры ядра

Теперь нам необходимо внести некоторые изменения в файл `sysctl`. Не стоит делать это путем редактирования `/etc/sysctl.conf`, лучше создать локальный конфиг. Обязателььно проверьте состояние параметров после настройки и перезагрузки `sysctl -a`.

```sh
cat <<EOF > /etc/sysctl.d/local.conf
# Параметр, чтобы включить переадресацию пакетов
net.ipv4.ip_forward=1

#Параметр, чтобы предотвратить MITM-атаки
net.ipv4.conf.all.accept_redirects = 0

#Параметр, чтобы запретить отправку ICMP-редиректов
net.ipv4.conf.all.send_redirects = 0

#Параметр, для запрета поиска PMTU
net.ipv4.ip_no_pmtu_disc = 1
EOF
```

```sh
sysctl -p
```

Настройка сетевых параметров завершена.

#### Настроим iptables

`iptables` — это утилита, которая управляет встроенным в Linux файрволом `netfilter`. Для того, чтобы сохранить правила `iptables` в файле и подгружать их при каждом запуске системы, установим пакет `iptables-persistent`:

```sh
apt-get install iptables-persistent
```

После установки нас спросят, сохранить ли текущие правила IPv4 и IPv6. Ответим «Нет», так как у нас новая система, и нечего сохранять.

Перейдем к формированию правил `iptables`. На всякий пожарный, очистим все цепочки:

```sh
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -F
iptables -Z
```

Разрешим соединения по SSH на 22 порту, чтобы не потерять доступ к машине:

```sh
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
```

Разрешим соединения на loopback-интерфейсе:

```sh
iptables -A INPUT -i lo -j ACCEPT
```

Теперь разрешим входящие соединения на UDP-портах 500 и 4500:

```sh
iptables -A INPUT -p udp --dport  500 -j ACCEPT
iptables -A INPUT -p udp --dport 4500 -j ACCEPT
```

Разрешим переадресацию ESP-трафика:

```sh
iptables -A FORWARD --match policy --pol ipsec --dir in  --proto esp -s 10.10.10.0/24 -j ACCEPT
iptables -A FORWARD --match policy --pol ipsec --dir out --proto esp -d 10.10.10.0/24 -j ACCEPT
```

Настроим маскирование трафика, так как наш VPN-сервер, по сути, выступает как шлюз между Интернетом и VPN-клиентами:

```sh
iptables -t nat -A POSTROUTING -s 10.10.10.0/24 -o eth0 -m policy --pol ipsec --dir out -j ACCEPT
iptables -t nat -A POSTROUTING -s 10.10.10.0/24 -o eth0 -j MASQUERADE
```

Настроим максимальный размер сегмента пакетов:

```sh
iptables -t mangle -A FORWARD --match policy --pol ipsec --dir in -s 10.10.10.0/24 -o eth0 -p tcp -m tcp --tcp-flags SYN,RST SYN -m tcpmss --mss 1361:1536 -j TCPMSS --set-mss 1360
```

Запретим все прочие соединения к серверу:

```sh
iptables -A INPUT -j DROP
iptables -A FORWARD -j DROP
```

Сохраним правила, чтобы они загружались после каждой перезагрузки:

```sh
netfilter-persistent save
netfilter-persistent reload
```

Настройка iptables завершена.

Чтобы проверить состояние, выключить или включить службу, соответственно:

```sh
systemctl status iptables.service
systemctl stop iptables.service
systemctl start iptables.service
```

Перезагрузим машину: `reboot`

И посмотрим работают ли правила iptables:

```sh 
sudo su
iptables -S
```

>...
-P INPUT ACCEPT
-P FORWARD ACCEPT
-P OUTPUT ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p udp -m udp --dport 500 -j ACCEPT
-A INPUT -p udp -m udp --dport 4500 -j ACCEPT
-A INPUT -j DROP
-A FORWARD -s 10.10.10.0/24 -m policy --dir in --pol ipsec --proto esp -j ACCEPT
-A FORWARD -d 10.10.10.0/24 -m policy --dir out --pol ipsec --proto esp -j ACCEPT
-A FORWARD -j DROP

Да, всё работает.

Работает ли strongSwan:

```sh
ipsec statusall
```

>...
Status of IKE charon daemon (strongSwan 5.7.2, Linux 4.19.0-14-amd64, x86_64):
  uptime: 71 seconds, since Mar 05 23:22:16 2022
...

Да, всё работает.

#### Настройка vpn клиента для Android


0. Передаем сгенерированный ранее ключ на устройство
	Файл контейнера сертификата мы сгенерировали ранее, он хранится в файле: `/etc/ipsec.d/p12/me.p12`
	Передаем его на устройство любым доступным способом: sd-карта, usb, bluetooth/
    На устройстве открываем файл, он должен определиться как сертификат, будет предложено ввести пароль. При успешной проверке пароля сертификат будет экспортирован в хранилище устройства и доступен для использования.

1. Настройка соединения
Можно воспользоваться встроенным в os механизмом, для этого в настройках в поиске вбить `vpn`. Откроется меню: `Другие настройки` -> `VPN`. Нужно зайти, открыть всплывающее меню и `Добавить профиль VPN`

	* Выбрать любое имя;
	* ввести адрес сервера;
	* выбрать тип соединения: IKEv2/IPSec RSA;
	* выбрать в полях Сертификат пользователя и Сертификат ЦС подгруженный "me"
	* выбрать в поле сертификат сервера: "Получено от сервера".
	* Сохранить

 В меню VPN появится пункт с добавленным именем. Нажатием на него активируется подключение.

2. Настройка соединения через Клиент StrongSwan

	* Устанавливаем его из магазина приложений;
	* нажимаем "Добавить профиль VPN;
	* Настройки аналогично предыдущему пункту
	* тип "IKEv2 Сертификат". Появится поле "Выбрать сертификат пользователя" оно не выглядит как кнопка, но является таковой.


#### Настройка vpn клиента для iOS

Создаем `.mobileconfig` для iPhone, iPad и Mac.

Мы будем использовать один и тот же VPN-профайл .mobileconfig для всех наших устройств.

Конфиг, который мы сделаем, устроен таким образом, чтобы инициировать соединение “On Demand”. Это означает, что при попытке любой службы или приложения выйти в Интернет, VPN-соединение будет всегда устанавливаться принудительно и автоматически. Таким образом, удастся избежать ситуации, когда вы забыли установить VPN-соединение, например, после перезагрузки девайса, а трафик в итоге пошел через провайдера, что нам совсем не нужно.

Скачаем скрипт, который сгенерирует для нас данный конфиг:

```sh
wget https://gist.githubusercontent.com/borisovonline/955b7c583c049464c878bbe43329a521/raw/b2d9dba73da633fcfcca6a03d877517c5b2d9485/mobileconfig.sh
```

Для того, чтобы скрипт отработал, нам потребуется пакет zsh, установим его:

Отредактируем название сервера по вкусу, а также пропишем внешний IP-адрес машины:

```sh
apt-get install zsh
nano mobileconfig.sh
SERVER="My NAME"
FQDN="YOUR_SERVER_IP"
```

Запустим скрипт и на выходе получим готовый файл `iphone.mobileconfig`:

```sh
chmod u+x mobileconfig.sh
./mobileconfig.sh > iphone.mobileconfig
```

Заберите этот файл с сервера, отправьте скачанный файл `iphone.mobileconfig` на все ваши устройства через Airdrop. Подтвердите на устройствах установку конфигурации.

В macOS профайл устанавливается из `System Preferences > Profiles`. В iOS он появится в `Settings > Profile Downloaded`.

Готово! Соединения с VPN-сервером установятся автоматически.

Если захочется временно отключить VPN, чтобы получить доступ, например, к Авито, в macOS зайдите в `System Preferences > Network`, выберите VPN-соединение и снимите галочку “Connect on Demand”, нажмите Apply.

В iOS: `Settings > General > VPN & Device Management > VPN >` нажмите на иконку “i” у установленной VPN конфигурации и выключите тумблер “Connect On Demand”. Чтобы вернуться обратно к автоматическому принудительному установлению соединений, соответственно, верните эти галки/тумблеры обратно.

Кстати, в macOS так же стоит поставить галку “Show VPN status in menu bar”. Будет удобно быстро чекать активно ли сейчас VPN-соединение.

