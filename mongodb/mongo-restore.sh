#!/bin/bash
# USAGE: ./mongo-restore.sh <TAR FILE> <DB NAME>

TMP_DIR="/tmp/mongorestore/"

MONGO_DOCKER_NAME='mongo_cnt'
MONGO_DOCKER_NET='back_default'
MONGO_DOCKER_IMAGE_VER='4.1.8-xenial'

MONGO_PORT_27017_TCP_ADDR=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $MONGO_DOCKER_NAME)
MONGO_PORT_27017_TCP_PORT=27017



rm -rf $TMP_DIR && mkdir $TMP_DIR
if [[ $1 =~ \.tar$ ]];
then
        #FILENAME=$(echo $1 | sed 's/.*\///')
        FILENAME=$2"/"
        mkdir $TMP_DIR
        echo "Data will be extracted into :"$TMP_DIR
        tar -C $TMP_DIR -xvf $1
else
        FILENAME=$(echo $1 | sed 's/.*\///')
        cp $1 $TMP_DIR$FILENAME
fi

docker run -it --rm --link $MONGO_DOCKER_NAME:mongo --net $MONGO_DOCKER_NET -v $TMP_DIR:/tmp mongo:$MONGO_DOCKER_IMAGE_VER bash -c "mongorestore --drop -v --host $MONGO_PORT_27017_TCP_ADDR:$MONGO_PORT_27017_TCP_PORT --db ""$2"" /tmp/""$FILENAME"
rm -rf $TMP_DIR