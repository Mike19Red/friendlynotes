# Шпаргалка mongodb

Сборная солянка команд и записей, которыми приходилось пользоваться. Самое ценное - создание/восстановление резервной копии. *Скрипты в каталоге.*



## Для базы запущенной в контейнере

```sh
docker exec -it mongo_cnt bash

mongo
use DB_NAME

```

## Работа с коллекциями

### Список коллекций

Получить список коллекций  ~~(таблиц)~~ в базе

```sh
db.getCollectionNames()
```


### Список записей коллекции `users`

```sh
db.users.find()
```

### Замена полей

Для первого вхождения в поле email такого значения установить поле `rights` в `admin`.

```sh
db.users.findOneAndUpdate({email:"email@email.email"},{$set:{rights:"admin"}})
```

### Вставить значение в коллекцию

```sh
db.users.find()
db.users.insert({ значение })

> WriteResult({ "nInserted" : 1 })
```

### Поиск пользователей, c полем `regDate` больше, чем дата

```sh
db.users.find({regDate:{$gt:ISODate("2013-03-22T06:22:51.422Z")}})
```

## backup

### Как создать и выгружать резервную копию

```sh
bash -x ./mongo-backup.sh DB_NAME /opt/backup/backup-33.12.2021.tar
bash -x ./mongo-restore.sh /opt/backup/backup-33.12.2021.tar DB_NAME

```

### Посмотреть данные из резервной копии в читаемом формате

```sh
bsondump --pretty --outFile=users.json users.bson
```

## Сделать резервную копию таблицы и обработать

```sh
mongoexport --db myDatabase --collection myCollection --jsonArray --out myJsonFile.json
```