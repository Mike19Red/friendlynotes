#!/bin/bash
# USAGE: ./mongo-backup.sh <DB NAME> <TAR FILE>


rm -rf /tmp/mongodump && mkdir /tmp/mongodump

MONGO_DOCKER_NAME='mongo_cnt'
MONGO_DOCKER_NET='back_default'
MONGO_DOCKER_IMAGE_VER='4.1.8-xenial'

MONGO_PORT_27017_TCP_ADDR=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $MONGO_DOCKER_NAME)
MONGO_PORT_27017_TCP_PORT=27017

docker run -it --rm --link $MONGO_DOCKER_NAME:mongo --net $MONGO_DOCKER_NET -v /tmp/mongodump:/tmp mongo:$MONGO_DOCKER_IMAGE_VER bash -c "mongodump -v --host $MONGO_PORT_27017_TCP_ADDR:$MONGO_PORT_27017_TCP_PORT --db "$1" --out=/tmp"

#docker run -it --rm --volumes-from mongo_cnt -v /tmp/mongodump:/tmp mongo:4.1.8-xenial bash -c 'mongodump -v --host $MONGO_PORT_27017_TCP_ADDR:$MONGO_PORT_27017_TCP_PORT --db '$1' --out=/tmp'
#docker run --rm --volumes-from mongo_cnt -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /data/db

tar -cvf $2 -C /tmp/mongodump . 
rm -rf /tmp/mongodump