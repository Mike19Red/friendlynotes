
# Сслыки на материалы

https://russianblogs.com/article/8036920964/
https://dev.to/ablx/monitoring-setup-with-docker-compose-part-3-alertmanager-35ln
https://mcs.mail.ru/docs/additionals/cases/cases-monitoring/case-node-exporter#

# Настройка и запуск

Не буду полностью дублировать [статью](https://russianblogs.com/article/8036920964/). Прочитайте её =Ь.

Чтобы запустить минимальный инстанс можно взять конфиги из каталога `configs`.

В конфиге `alertmanager.yml` установите свой адрес электронной почты и пароль для доступа к ней. Если почта не от google, поменяйте smtp сервер.

В конфиге `prometheus.yml` установите свой IP адрес для мониторинга.

Установить и запустить можно как в примере:

```sh
mkdir -p /usr/local/src/config
cp configs/* /usr/local/src/config
cd /usr/local/src/config

# Запустить контейнер:
docker-compose -f docker-compose-monitor.yml up -d

# Удалить контейнер:
docker-compose -f docker-compose-monitor.yml down

# Перезагрузите контейнер:
docker restart id

```

При запуске всех контейнеров корректно в колонке `STATUS` команды `docker ps` должен быть статус `UP`.

Если контейнеры перезапускаются, лезьте в шпаргалку по докеру и наслаждайтесь отладкой. Скорее всего для какого-то из контейнеров некорректные настройки.

