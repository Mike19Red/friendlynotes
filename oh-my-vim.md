# Oh-my-vim


## Установка и первый запуск.

При первом запуске будут скачаны и настроены плагины.

```sh
apt-get install fonts-powerline

ln -sf bash /bin/sh
pip install fuzzy-matcher
curl -L https://raw.github.com/liangxianzhe/oh-my-vim/master/tools/install.sh | bash
vim
```

## Команды и фичи

