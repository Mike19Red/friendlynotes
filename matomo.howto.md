## Описание


`MATOMO` - условно-бесплатная система для мониторинга действий пользователей на сайте.

Ранее известна как Piwik.

Рассматривалась как один из вариантов замены бесплатным сервисам yandex и google аналитики.
Указанные сервисы собирают персональные данные, что недопустимо во многих проектах.

Может быть развернут полностью на своем сервере. Есть возможность развернуть мониторинг в облаке компании за плату. Функциональных различий нет. При размещении на серверах компании Matomo есть служба поддержки и не требуется технических знаний.

Далее инструкции по запуску на локальном сервере, не в prod режиме.

## Ссылки

- Основной сайт проекта

	https://matomo.org/feature-overview/

- Оффициальный ман по установке

    https://matomo.org/docs/installation/

- Страница с оффициальным образом. Нет compose файла

    https://hub.docker.com/_/matomo

- Репо основного образа. Есть compose файлы для nginx в папке .examples

    https://github.com/matomo-org/docker

- Инструкция, взятая за основу для локального развертывания

    https://medium.com/@davquar/matomo-lets-install-it-with-docker-512211705c76

- Инструкция по установке, но для специфичного хостера

    https://docs.bytemark.co.uk/article/how-to-setup-matomo-using-docker/

- "Сложный" вариант настройки с помощью обратного прокси-сервера traefic

    https://raw.githubusercontent.com/BytemarkHosting/configs-matomo-docker/master/docker-compose.yml
    https://habr.com/ru/post/508636/

- Подключение matomo к nodejs проектам

    https://matomo.org/blog/2014/06/track-api-calls-node-js-piwik/
    https://openbase.com/js/matomo-tracker/documentation
    https://github.com/matomo-org/matomo-nodejs-tracker


## Запуск


Настройка `nginx-proxy`:

https://hub.docker.com/r/jwilder/nginx-proxy/

```sh
docker network create nginx-proxy

docker run -d -p 8000:80 -v /var/run/docker.sock:/tmp/docker.sock:ro \
    --name nginx-proxy --net nginx-proxy jwilder/nginx-proxy

docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
```

- https://medium.com/@davquar/matomo-lets-install-it-with-docker-512211705c76


Содержимое файла `docker-compose.yml`:

```
version: "2"

services:
  matomo:
    container_name: matomo
    image: matomo
    ports:
      - 8080:80
    environment:
      - MATOMO_DATABASE_HOST=matomo_db
      - VIRTUAL_HOST=matomo.local
      - LETSENCRYPT_HOST=matomo.local
      - LETSENCRYPT_EMAIL=email@something.ext
    env_file:
      - ./db.env
    networks:
      - proxy
      - net
    depends_on:
      - matomo_db
    restart: unless-stopped

  matomo_db:
    container_name: matomo_db
    image: mariadb
    command: --max-allowed-packet=64MB
    environment:
      - MYSQL_ROOT_PASSWORD=makeitup
    env_file:
      - ./db.env
    networks:
      - net
    restart: unless-stopped

networks:
  proxy:
    external:
      name: nginx-proxy
  net:
    driver: bridge
```

Содержимое файла `db.env`:

```
MYSQL_PASSWORD=makeitup2
MYSQL_DATABASE=matomo
MYSQL_USER=matomo
MATOMO_DATABASE_ADAPTER=mysql
MATOMO_DATABASE_TABLES_PREFIX=matomo_
MATOMO_DATABASE_USERNAME=matomo
MATOMO_DATABASE_PASSWORD=
MATOMO_DATABASE_DBNAME=matomo
```

Осталось запустить `docker-compose up -d`.

Страница доступна на http://localhost:8080/

Note that at this point we should already have an active and working SSL certificate for our nice subdomain.

- Super user login:
  `deadbeaf`

- password:
  `deadbeaf17`

```
Tracking code for local.project
Email these instructions

To track your web traffic with Matomo you need to make sure some extra code is added to each of your webpages.

In most websites, blogs, CMS, etc. you can use a pre-made plugin to do the technical work for you. (See our list of plugins used to integrate Matomo.)
If no plugin exists you can edit your website templates and add the JavaScript tracking code to the </head> tag which is often defined in a 'header.php', 'header.tpl' or similar template file.
JavaScript Tracking Code

Make sure this code is on every page of your website. We recommend pasting it immediately before the closing </head> tag.

<!-- Matomo -->
<script>
  var _paq = window._paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//172.18.0.3/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->

If you want to do more than track page views, please check out the Matomo Javascript Tracking documentation for the list of available functions. Using these functions you can track goals, custom variables,
ecommerce orders, abandoned carts and more.


Note: After the installation process, you can generate customized tracking code in the Tracking Code admin section.
Log Analytics

If the Javascript tracking method isn’t feasible, you can use server log analytics as an alternative method for tracking your website’s users.
Mobile apps and SDKs

Not tracking a website? You can alternatively track a mobile app or any other type of application using one of the available SDKs.
HTTP Tracking API

The HTTP Tracking API allows you to track anything. This may be useful if you are using a programming language for which no SDK exists yet. It may also be useful when you want to track devices or application in a special way.

Help for users with high traffic websites

For medium and high traffic websites there are certain optimizations that should be made to help Matomo run faster (such as setting up auto-archiving).

Read this to learn more.


For a proper geolocation Matomo requires an external database. Using this option, Matomo will automatically be configured to download and use the latest dbip city level database. [View licensing terms]
When users have set their web browser to "I do not want to be tracked" (DoNotTrack is enabled) then Matomo will not track these visits.
When users visit your website, Matomo will not use the full IP address (such as 213.34.51.91) but instead Matomo will anonymize it first (to 213.34.0.0). IP address anonymisation is one of the requirements set by the privacy laws in some countries such as Germany.
```
